﻿Drupal.behaviors.eiti = {
  attach: function (context, settings) {
    //jQuery('#views-bootstrap-grid-1 .views-field-title a').dotdotdot();
    //jQuery('#block-views-eiti-block-1 .views-field-title a').dotdotdot({ellipsis  : '... '});
    //jQuery('#block-views-eiti-block-1 .views-field-title a').dotdotdot();
    //jQuery('#block-views-eiti-block-1 .views-field-title a').addClass('ellipsis');

    jQuery('.facebook a img').attr("src","/sites/all/themes/eiti_theme/css/images/mockup_eiti_03.png");
    jQuery('.twitter a img').attr("src","/sites/all/themes/eiti_theme/css/images/mockup_eiti_04.png");
    jQuery('.googleplus a img').attr("src","/sites/all/themes/eiti_theme/css/images/mockup_eiti_06.png");
    jQuery('.instagram a img').attr("src","/sites/all/themes/eiti_theme/css/images/mockup_eiti_08.png");
    jQuery('.pinterest a img').attr("src","/sites/all/themes/eiti_theme/css/images/mockup_eiti_05.png");
    jQuery('.vimeo a img').attr("src","/sites/all/themes/eiti_theme/css/images/mockup_eiti_07.png");
    jQuery('.youtube a img').attr("src",'/sites/all/themes/eiti_theme/css/images/mockup_eiti_09.png');
    if(jQuery('.flicker').length == 0){
        jQuery('.social-media-links .last').after('<li class="flicker"><a href="https://www.flickr.com/photos/minminasco" target="_blank" title="flicker"><img src="/sites/all/themes/eiti_theme/css/images/fk.png" alt="buscar icon"></a></li>');
    }

    if(jQuery("#edit-field-tipo-tid").val() ==10){
        jQuery('.view-documentos .view-header p').text('Las reuniones del Comité Tripartito Nacional son sistematizadas, aprobadas y firmadas por los miembros. Consulte las diferentes actas:');
    }

    jQuery('#quicktabs-tab-view__documentos__page-1').click(function(){
        jQuery('.view-documentos .view-header p').text('Las reuniones del Comité Tripartito Nacional son sistematizadas, aprobadas y firmadas por los miembros. Consulte las diferentes actas:');
    });

    jQuery('#quicktabs-tab-view__documentos__page-2').click(function(){
        jQuery('.view-documentos .view-header p').text('Desde 2013, Colombia inicia acciones con el fin de implementar la iniciativa EITI en Colombia. Encuentre acá los documentos para la participación de Colombia en EITI.');
    });

    jQuery('#quicktabs-tab-view__documentos__page-3').click(function(){
        jQuery('.view-documentos .view-header p').text('Con el fin de proveer información veraz, oportuna, contextualizada y socialmente útil para fortalecer la transparencia en la cadena de valor del sector extractivo en beneficio del desarrollo sostenible local y nacional, así como dar cumplimiento s los requisitos establecidos por el Estándar, Colombia formuló e implementa el Plan de Acción Nacional.- PAN. Encuentre acá el Plan de Acción Nacional.');
    });

    jQuery('#quicktabs-tab-view__documentos__page-4').click(function(){
        jQuery('.view-documentos .view-header p').text('Anualmente, la Iniciativa EITI en Colombia, presenta ante la Secretaría Internacional de EITI, informes sobre las Gestiones adelantadas. Encuentre acá los informes para las vigencias 2015 y 2016.');
    });

    jQuery('#quicktabs-tab-view__documentos__page-5').click(function(){
        jQuery('.view-documentos .view-header p').text('Con el fin de darle cumplimiento a las actividades expuestas en el Plan de Acción Nacional- PAN se han llevado a cabo diferentes estudios y consultorías. Consulte los resultados de estudios y consultorías:');
    });

    jQuery('#quicktabs-tab-view__documentos__page-6').click(function(){
        jQuery('.view-documentos .view-header p').text('Acá se podrán consultar documentos de interés sobre el funcionamiento de la iniciativa en Colombia');
    });

    jQuery('.high_contrast_switcher_high').click(function(){
        jQuery('.high_contrast_switcher_high').css('display', 'none');
        jQuery('.high_contrast_switcher_normal').css('display', 'inline-block');
        jQuery('.high_contrast_switcher_normal').css('backgroud-image', 'url("/sites/default/files/iconContraste.png")')
    });

    jQuery('.high_contrast_switcher_normal').click(function(){
        jQuery('.high_contrast_switcher_normal').css('display', 'none');
        jQuery('.high_contrast_switcher_high').css('display', 'inline-block');
        jQuery('.high_contrast_switcher_high').css('backgroud-image', 'url("/sites/default/files/iconContraste.png")')
    });

    jQuery('.quicktabs-style-nostyle').addClass('nav nav-tabs');
    jQuery('#quicktabs-tab-view__documentos__page-0').parent().remove();
    jQuery('#views-bootstrap-grid-1 .col-lg-6 .views-field-view-node a').attr('href', '/tags/noticias');
    //Toogle
    jQuery("[data-toggle=popover]").popover();
    jQuery('#print').click(function(event){
      window.print();
    });

    jQuery(".view-documentos #views-bootstrap-media-1 .media-list ol" ).after( "<a href='node/147' class='btn btn-default action'>Ir a la herramienta EITI</a>" ); }
};
