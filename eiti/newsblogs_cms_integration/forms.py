
from aldryn_newsblog.forms import AutoAppConfigFormMixin
from django import forms

from newsblogs_cms_integration.models import NewsBlogLatestArticlesWithFiltersPlugin


class NewsBlogLatestArticlesWithFiltersPluginForm(AutoAppConfigFormMixin,
                                       forms.ModelForm):
    class Meta:
        model = NewsBlogLatestArticlesWithFiltersPlugin
        fields = [
            'app_config', 'latest_articles', 'exclude_featured',
            'cache_duration','categories'
        ]
