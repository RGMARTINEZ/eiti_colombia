/*!
*   Accessibility Buttons v3.1.2
*   http://tiagoporto.github.io/accessibility-buttons
*   Copyright (c) 2014-2017 Tiago Porto (http://tiagoporto.com)
*   Released under the MIT license
*/

"use strict";

/**
 * accessibilityButtons
 * @param  {Array}  -
 * @return
 */

/* exported accessibilityButtons */
var accessibilityButtons = function accessibilityButtons(options) {
  'use strict';
  /**
   * hasClass
   * @param  {string}  element - DOM element
   * @param  {string}  clazz   - Class Name
   * @return {Boolean}
   */

  function hasClass(element, clazz) {
    return " ".concat(element.className, " ").indexOf(" ".concat(clazz, " ")) > -1;
  }

  var setting = {
    font: {
      nameButtonIncrease: '<i class="fa fa-font" aria-hidden="true"></i>',
    ariaLabelButtonIncrease: 'Increase Font',
      nameButtonDecrease: '<i class="fa fa-font" aria-hidden="true"></i>',
    ariaLabelButtonDecrease: 'Decrease Font'
    },
    contrast: {
      nameButtonAdd: '<i class="contrast"></i> ',
      ariaLabelButtonAdd: '',
      nameButtonRemove: '<i class="contrast"></i> ',
      ariaLabelButtonRemove: ''
    }
  }; // Set buttons name and aria label

  if (options) {
    for (var key in options) {
      if (options.hasOwnProperty(key)) {
        var obj = options[key];

        for (var prop in obj) {
          if (obj.hasOwnProperty(prop)) {
            setting[key][prop] = obj[prop];
          }
        }
      }
    }
  }

  var $body = document.body,
      $fontButton = document.getElementById('accessibility-font'),
      $contrastButton = document.getElementById('accessibility-contrast'),
      $accessibilityButtons = document.getElementsByClassName('js-accessibility'),
      storageFont = localStorage.accessibility_font,
      storageContrast = localStorage.accessibility_contrast; // Check if exist storage and set the correct button names and aria attributes


  /**
   * Get the click event
   * Rename the buttons
   * Apply/Remove Contrast or Font Size
   * Manage storage
   */


  function accessibility() {
    return function () {
      var $this = this;




      if ($this.getAttribute('id') === 'accessibility-font'){
        $body.classList.remove('accessibility-font-increase');
        $body.classList.remove('accessibility-font-decrease');

                if ($this.getAttribute('id') === 'accessibility-font') {
          //$this.innerHTML = setting.font.nameButtonIncrease;
          //$this.setAttribute('aria-label', setting.font.ariaLabelButtonIncrease);
          localStorage.removeItem('accessibility_font');
        }

      }

          if (hasClass($body, 'accessibility-contrast')) {
      $body.classList.remove('accessibility-contrast');
       if ($this.getAttribute('id') === 'accessibility-contrast') {
          //$this.innerHTML = setting.font.nameButtonIncrease;
          //$this.setAttribute('aria-label', setting.font.ariaLabelButtonIncrease);
          localStorage.removeItem('accessibility_contrast');
        }



      }







      else {

      if ($this.getAttribute('id') != 'accessibility-font') {
      $body.classList.add($this.getAttribute('id'));
      }

        if ($this.getAttribute('id') === 'accessibility-font') {
          if (!storageFont) {
            localStorage.setItem('accessibility_font', true);
          }

          //$this.innerHTML = setting.font.nameButtonDecrease;
          //$this.setAttribute('aria-label', setting.font.ariaLabelButtonDecrease);
        } else {
          if (!storageContrast) {
            localStorage.setItem('accessibility_contrast', true);
          }

          //$this.innerHTML = setting.contrast.nameButtonRemove;
          //$this.setAttribute('aria-label', setting.contrast.ariaLabelButtonRemove);
        }
      }
    };
  } // Listening Click Event


  for (var i = 0; i < $accessibilityButtons.length; i++) {
    $accessibilityButtons[i].addEventListener('click', accessibility());
  }
};