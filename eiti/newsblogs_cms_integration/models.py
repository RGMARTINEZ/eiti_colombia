# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from aldryn_categories.fields import CategoryManyToManyField
from aldryn_newsblog.models import Article, PluginEditModeMixin, AdjustableCacheModelMixin, NewsBlogCMSPlugin
from aldryn_newsblog.utils.utilities import get_valid_languages_from_request
from cms.models import PlaceholderField, Model, python_2_unicode_compatible, CMSPlugin
from django.db import connection, models
from django.utils.translation import ugettext
from django.utils.translation import ugettext_lazy as _


class ArticleExtension(models.Model):
    article = models.OneToOneField(Article, related_name='extension')
    video_placeholder = PlaceholderField('article_video',
                               related_name='article_video')






@python_2_unicode_compatible
class NewsBlogLatestArticlesWithFiltersPlugin(PluginEditModeMixin,
                                   AdjustableCacheModelMixin,
                                   NewsBlogCMSPlugin):
    latest_articles = models.IntegerField(
        default=5,
        help_text=_('The maximum number of latest articles to display.')
    )



    exclude_featured = models.PositiveSmallIntegerField(
        default=0,
        blank=True,
        help_text=_(
            'The maximum number of featured articles to exclude from display. '
            'E.g. for uses in combination with featured articles plugin.')
    )

    categories = CategoryManyToManyField('aldryn_categories.Category',
                                         verbose_name=_('categories'),
                                         blank=True)


    def copy_relations(self, oldinstance):
        self.categories = oldinstance.categories.all()
        # for article in oldinstance.categories.all():
        #
        #     article.pk = None
        #     article.plugin = self
        #     article.save()


    def get_articles(self, request):
        """
        Returns a queryset of the latest N articles. N is the plugin setting:
        latest_articles.
        """
        print request

        queryset = Article.objects
        featured_qs = Article.objects.all().filter(is_featured=True)

        if not self.get_edit_mode(request):
            queryset = queryset.published()
            featured_qs = featured_qs.published()
        languages = get_valid_languages_from_request(
            self.app_config.namespace, request)
        if self.language not in languages:
            return queryset.none()
        queryset = queryset.translated(*languages).filter(
            app_config=self.app_config)
        featured_qs = featured_qs.translated(*languages).filter(
            app_config=self.app_config)
        exclude_featured = featured_qs.values_list(
            'pk', flat=True)[:self.exclude_featured]
        queryset = queryset.exclude(pk__in=list(exclude_featured))

        queryset = queryset.filter(pk__in = list(set(list(self.categories.all().values_list('article', flat=True)))))

        return queryset[:self.latest_articles]

    def __str__(self):
        return ugettext('%(app_title)s latest articles: %(latest_articles)s') % {
            'app_title': self.app_config.get_app_title(),
            'latest_articles': self.latest_articles,
        }
