<?php
  $text_resize = module_invoke('text_resize', 'block_view', '0');
  $high_contrast = module_invoke('high_contrast', 'block_view', 'high_contrast_switcher');
?>
<!--<button class="glyphicon glyphicon-globe" role="button" data-html="true" data-toggle="popover" data-trigger="focus" data-container="body" data-content='<?php print render($text_resize['content']);?><button class="glyphicon glyphicon-print" id="print"></button><?php print render($high_contrast['content']);?>' data-placement="bottom"></button>-->

<div class="text-resize">
	<?php print render($text_resize['content']);?> 
</div>
<div class="contrast">
	<?php print render($high_contrast['content']);?>
</div>