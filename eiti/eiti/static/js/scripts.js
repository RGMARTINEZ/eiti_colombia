/*!
*   Accessibility Buttons v3.1.2
*   http://tiagoporto.github.io/accessibility-buttons
*   Copyright (c) 2014-2017 Tiago Porto (http://tiagoporto.com)
*   Released under the MIT license
*/
function ready(fn) {
  if (document.readyState !== 'loading') {
      fn();
  } else {
      document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(function() {
  accessibilityButtons();
});