"""
WSGI config for eiti project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os, sys

sys.path.append('/opt/eiti/eiti')
sys.path.append('/home/super/.virtualenvs/eiti/lib/python2.7/site-packages')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "eiti.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
