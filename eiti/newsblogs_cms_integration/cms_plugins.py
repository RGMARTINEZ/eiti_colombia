from aldryn_newsblog import forms
from aldryn_newsblog.cms_plugins import AdjustableCacheMixin
from aldryn_newsblog.models import NewsBlogLatestArticlesPlugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _

from newsblogs_cms_integration.forms import NewsBlogLatestArticlesWithFiltersPluginForm
from newsblogs_cms_integration.models import NewsBlogLatestArticlesWithFiltersPlugin



@plugin_pool.register_plugin
class NewsBlogLatestArticlesPlugin3(AdjustableCacheMixin, CMSPluginBase):
    render_template = 'newsblogs_cms_integration/latest_articles3.html'
    name = _('Latest Articles 3')
    model = NewsBlogLatestArticlesPlugin
    form = forms.NewsBlogLatestArticlesPluginForm

    def render(self, context, instance, placeholder):
        request = context.get('request')
        context['instance'] = instance
        context['article_list'] = instance.get_articles(request)
        return context


@plugin_pool.register_plugin
class NewsBlogLatestWithFiltersArticlesPlugin(CMSPluginBase,AdjustableCacheMixin):
    render_template = 'newsblogs_cms_integration/latest_articles4.html'
    name = _('Latest Articles By Categories')
    model = NewsBlogLatestArticlesWithFiltersPlugin
    form = NewsBlogLatestArticlesWithFiltersPluginForm

    def render(self, context, instance, placeholder):
        request = context.get('request')
        context['instance'] = instance
        context['article_list'] = instance.get_articles(request)
        return context

@plugin_pool.register_plugin
class NewsBlogLatestWithFiltersFormationPlugin(CMSPluginBase,AdjustableCacheMixin):
    render_template = 'newsblogs_cms_integration/list_formation.html'
    name = _('Lista de Programa de Formacion')
    model = NewsBlogLatestArticlesWithFiltersPlugin
    form = NewsBlogLatestArticlesWithFiltersPluginForm

    def render(self, context, instance, placeholder):
        request = context.get('request')
        context['instance'] = instance
        context['article_list'] = instance.get_articles(request)
        return context


@plugin_pool.register_plugin
class NewsBlogLatestVideosPlugin(AdjustableCacheMixin, CMSPluginBase):
    render_template = 'newsblogs_cms_integration/latest_videos.html'
    name = _('Latest Videos')
    model = NewsBlogLatestArticlesWithFiltersPlugin
    form = NewsBlogLatestArticlesWithFiltersPluginForm
    cache = False

    def render(self, context, instance, placeholder):
        request = context.get('request')
        context['instance'] = instance
        context['article_list'] = instance.get_articles(request)
        return context



@plugin_pool.register_plugin
class CarouselNews(AdjustableCacheMixin, CMSPluginBase):
    render_template = 'newsblogs_cms_integration/carousel.html'
    name = _('Carousel News')
    model = NewsBlogLatestArticlesPlugin
    form = forms.NewsBlogLatestArticlesPluginForm

    def render(self, context, instance, placeholder):
        request = context.get('request')
        context['instance'] = instance
        context['article_list'] = instance.get_articles(request)
        return context