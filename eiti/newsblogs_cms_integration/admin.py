# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from cms.admin.placeholderadmin import PlaceholderAdminMixin
from django.contrib import admin

# Register your models here.
from newsblogs_cms_integration import models


class ArticleExtensionAdmin(PlaceholderAdminMixin,admin.ModelAdmin):
    pass

admin.site.register(models.ArticleExtension, ArticleExtensionAdmin)